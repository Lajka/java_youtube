CREATE TABLE data_sources (
       id serial PRIMARY KEY,
       name text NOT NULL,
       method text NOT NULL, -- json xml
       address text NOT NULL
);

CREATE TABLE user_data_sources (
       id serial PRIMARY KEY,
       source integer REFERENCES data_sources (id)
);

CREATE TABLE user_preferences (
       id serial PRIMARY KEY,
       sources integer REFERENCES user_data_sources (id)
);

CREATE TABLE categories (
       id serial PRIMARY KEY,
       name text NOT NULL,
       description text
);

CREATE TABLE keywords (
       id serial PRIMARY KEY,
       name text NOT NULL
);

CREATE TABLE users (
       id serial PRIMARY KEY,
       user_name text NOT NULL,
       pw_hash text NOT NULL,
       pw_client_hash text NOT NULL,
       pw_salt text NOT NULL,
       pw_client_salt text NOT NULL,
       first_name text,
       last_name text,
       last_login timestamp,
       preferences integer REFERENCES user_preferences (id)
);

CREATE TABLE user_keywords (
       id serial PRIMARY KEY,
       keyword integer REFERENCES keywords (id)
);

CREATE TABLE link_keywords (
       id serial PRIMARY KEY,
       keyword integer REFERENCES keywords (id)
);

CREATE TABLE links (
       id serial PRIMARY KEY,
       name text NOT NULL,
       category integer REFERENCES categories (id),
       keywords integer REFERENCES link_keywords (id),
       source integer REFERENCES data_sources (id)
);

CREATE TABLE user_links (
       id serial PRIMARY KEY,
       links integer REFERENCES links (id)
);

CREATE TABLE videos (
    id serial PRIMARY KEY,
    thumbnail_url varchar(256) NOT NULL,
    video_url varchar(256) NOT NULL,
    description varchar(500),
    title varchar(256) NOT NULL,
    duration varchar(20),
    searchterm varchar(256) NOT NULL,
    video_id varchar(50) NOT NULL UNIQUE
);




