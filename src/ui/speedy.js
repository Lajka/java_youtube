var speedy = (function () {
    var app = {};
    // Module public components
    //

	var col_id_counter = 0;
	var row_id_counter = 0;
	app.getFrontPage = function () { 
		var video_promise = getFrontPageVideos();

		var res = video_promise.then( function( response ) {
			// check that we got an array

			var data = response.data;

			if ( data.length != 6 ) {
				console.log("Data length mismatch");
				return;
			}

			for ( var i = 0; i < 2; i++ ) {
				// var row_id = "row_" + i;
				var row_id = "row_" + row_id_counter++;

				$("#videoContent").append('<div class="row" id=' + row_id + '>');

				for ( var j = 0; j < 3; j++ ) {
					var title = data[3 * i + j].title;
					var thumb = data[3 * i + j].thumbnailUrl;
					var video = data[3 * i + j].videoUrl;
					$(('#' + row_id)).append('<div class="col-md-4 content" id="col_' + 3*i+j + '">' + 
											 '<h4>' +  title + '</h4>' +
											 '<a href=' + video + '>' +
											 '<img src=' + thumb + ' class="img-rounded">' + '</img>' +
											 '</a>' + 
											 '</div>');
				}
				$("#videoContent").append('</div>');
			}
		});

		

		video_promise.catch( function( error ) {
			console.log( error );
		}).done();

	}

	app.searchVideos = function( term ) {
			var video_promise = searchVideo( term );

					var res = video_promise.then( function( response ) {
			// check that we got an array

			var data = response.data;

			if ( data.length != 6 ) {
				console.log("Data length mismatch");
				return;
			}

			for ( var i = 0; i < 2; i++ ) {
				//var row_id = "row_" + i;
				var row_id = "row_" + row_id_counter++;

				$("#videoContent").append('<div class="row" id=' + row_id + '>');

				for ( var j = 0; j < 3; j++ ) {
					var title = data[3 * i + j].title;
					var thumb = data[3 * i + j].thumbnailUrl;
					var video = data[3 * i + j].videoUrl;
					$(('#' + row_id)).append('<div class="col-md-4 content" id="col_' + 3*i+j + '">' + 
											 '<h4 class="text-muted">' +  title + '</h4>' +
											 '<a href=' + video + '>' +
											 '<img src=' + thumb + ' class="img-rounded">' + '</img>' +
											 '</a>' + 
											 '</div>');
				}
				$("#videoContent").append('</div>');
			}
		});

		

		video_promise.catch( function( error ) {
			console.log( error );
		}).done();
		}

    app.login = function () {
		var userName     = document.getElementById("login_username");
		var userPassword = document.getElementById("login_password");

		// getClientSaltQuery gets the client salt,, this needs to be stored.
		// 
		var L = 64;
		var N = 16384;
		var r = 8;
		var p = 1;
		
		var key;
		var keyL1;
		
		var salt_promise = getClientSaltQuery( userName.value );

		var a_res = salt_promise.then( function( response ) {
			console.log( "And here: " + userName.value );	    
			var userPasswordU8    = scrypt.encode_latin1( userPassword.value );
			var clientSaltU8      = scrypt.encode_latin1( response.clientSalt );

			key = scrypt.crypto_scrypt( userPasswordU8, clientSaltU8, N, r, p, L );
			keyL1 = scrypt.decode_latin1( key );

			loginQuery( userName.value, response.clientSalt, keyL1 );
			console.log( "Aaaand here: " + userName.value );
		});

		salt_promise.catch( function( error ) {
			console.log( error );
		}).done();
	}

    app.getClientSalt = function( name ) {
		return getClientSaltQuery( name );
    }
    
    app.registerUser = function () {
		var userName     = document.getElementById("registeration_username");
		var userPassword = document.getElementById("registeration_password");
		
		if ( !validateUserName(userName) ) {
			console.log("INVALID USERNAME");
			// invalid user name
		}

		if ( !validateUserPassword(userPassword) ) {
			console.log("INVALID USER PASSWORD");
			// invalid user password
		}
		
		var userPasswordSalt = scrypt.encode_latin1(generateSalt());
		var userPasswordU8   = scrypt.encode_latin1(userPassword.value);
		var L = 64;
		var N = 16384;
		var r = 8;
		var p = 1;

		var key  = scrypt.crypto_scrypt( userPasswordU8, userPasswordSalt, N, r, p, L );
		var keyL1 = scrypt.decode_latin1( key );
		var saltL1 = scrypt.decode_latin1( userPasswordSalt );

		registerUserQuery( userName.value, saltL1, keyL1 ).then(
			function( response ) {
				console.log( "registeration succeeded" );
			}
		).fail(
			function( response ) {
				console.log("registeration failed");
			}
		).done();
    }

    app.userExists = function( userName ) {
		if ( !validateUserName(userName) ) {
			// invalid user name
			console.log("ha");
		}

		userExistsQuery( user );
    }

    app.dropdown = $(function() {
		$('.dropdown-toggle').dropdown();
		
		$('.dropdown input, .dropdown label').click(function(e) {
			e.stopPropagation();
		});
    });

    //
    // Module private components 
    //
    var scrypt = scrypt_module_factory();

    function generateSalt() {
		return Math.random().toString(35).replace(/[^a-z]+/g, '').substring(0, 8);
    }

	log_error = true;

	app.showLoginError = function() {
		if (log_error == true) {
			log_error = false;
			loginError();
		} else {
			log_error = true;
			loginError_off();
		}
	}

	reg_error = true;

	app.showRegError = function() {
		if (reg_error == true) {
			reg_error = false;
			registerationError();
		} else {
			reg_error = true;
			registerationError_off();
		}
	}

	function loginError() {
		$("#login").append( '<div id="login_error" class="alert-box error"><span>error: </span>Write your error message here.</div>' )
	}

	function loginError_off() {
		$("#login_error").animate({height: "0px"}, {duration: 200, 
													 complete: function() { $("#login_error").remove() } } );
	}

	function registerationError() {
		$("#registeration").append( '<div id="registeration_error" class="alert-box error"><span>error: </span>Käyttäjätunnus on jo käytössä</div>' );
	}

	function registerationError_off() {
		$("#registeration_error").animate({height: "0px"}, {duration: 100,
														  complete: function() { $("#registeration_error").remove() } } );
	}

    function validateUserName( userName  ) {
      if ( userName.length < 4 ) {
        // Minimum user name length is 4 characters
        return false;
      }

      if ( userName.length > 32 ) {
        // Maximum user name length is 32 characters
        return false;
      }

      var charsNotOk = _.find(userName,
             function(c) {
               switch (c) {
                 case '!': case '"': case '#': case '¤': case '%': case '&': case '/': case '(': case ')':
                 case '=': case '+': case '?': case '`': case '´': case '\'': case '*': case '^': case '¨':
                 case '~': case '<': case '>': case '|': case ',': case ';': case ':': case '½': case '§':
                 case '@': case '£': case '$': case '€': case '{': case '[': case ']': case '}': case '\\':
                 {
                   return true;
                   break;
                 }
                 default:
                 {
                  return false;
                 }
               }

             }
          );

      if ( charsNotOk ) {
          return false;
      }
		
		return true;
    }

    function validateUserPassword( userPassword ) {
		if ( userPassword.length < 4 ) {
			// Minimum password length is 4 characters
			return false;
		}

		if ( userPassword.length > 64 ) {
			// Maximum password length is 64 characters
			return false;
		}

		return true;
    }

    var serverAddress = "http://localhost:8000";

    function getSettingsQuery () {

		var r;

		Q($.ajax({
			url: serverAddress,
			dataType: "jsonp",
			crossDomain: true,
			data: {
				t: "getSettings",
			},
			success: function ( response ) {
				r = response;
			}
		})).then(
			function() {
				return r;
			}
		).fail(
			function() {
				new Error( "Get settings failed!" );
			}
		);
    }

    function setSettingsQuery() {

		var r;

		Q($.ajax({
			url: serverAddress,
			dataType: "jsonp",
			crossDomain: true,
			data: {
				t: "setSettings",
			},
			success: function ( response ) {
				r = response;
			}
		})).then(
			function() {
				return r;
			}
		).fail(
			function() {
				new Error( "Set settings failed!" );
			}
		);
    }

    function loginQuery ( user, salt, password ) {

		var r = Q.defer();
		$.ajax({
			url: serverAddress,
			dataType: "jsonp",
			crossDomain: true,
			data: {
				t: "login",
				u: user,
				s: salt,
				p: password
			},
			success: function ( response ) {
				r.resolve( response );
			},

			error: function( response ) {
				r.reject( response );
			}
		});
		return r.promise;
    }

    function registerUserQuery( user, salt, password ) {
		var r = Q.defer();

		$.ajax({
			url: serverAddress,
			dataType: "jsonp",
			crossDomain: true,
			data: {
				t: "registerUser",
				u: user,
				s: salt,
				p: password
			},
			success: function( response ) {
				r.resolve( response );
			},
			error: function( response ) {
				r.reject( response );
			}
		});

		return r.promise;
    }

    function userExistsQuery( user ) {

		var r = Q.defer();

		$.ajax({
            url: serverAddress,
            dataType: "jsonp",
			crossDomain: true,
            data: {
				t: "userExists",
				u: user
            },
            success: function( response ) {
				r.resolve( response );
            },
			
			error: function ( response ) {
				r.reject( response );
			}
		});
		return r.promise;
    }

	// app.getClientSaltQuery = speedy.promisedQuery( ,{t:"getClientSalt", u:user})
    function getClientSaltQuery( user ) {
		var r = Q.defer();

		$.ajax({
			url: serverAddress,
			dataType: "jsonp",
			crossDomain: true,
			data: {
				t: "getClientSalt",
				u: user
			},
			success: function( response ) {
				r.resolve( response );
			},
			error: function( response ) {
				r.reject( response );
			}
		});
		return r.promise;
    }

	function getVideo( videoId ) {
		var r = Q.defer();
		
		$.ajax({
			url: serverAddress,
			dataType: "jsonp",
			crossDomain: true,
			data: {
				t: "getVideo",
				i: videoId
			},
			success: function( response ) {
				r.resolve( response );
			},
			error: function( response ) {
				r.reject( response );
			}
		});
		
		return r.promise;
	}

	app.getVideo = getVideo;
	app.searchVideo = searchVideo;

	function searchVideo( searchTerm ) {
		var r = Q.defer();

		$.ajax({
			url: serverAddress,
			dataType: "jsonp",
			crossDomain: true,
			data: {
				t: "searchVideo",
				st: searchTerm
			},
			success: function( response ) {
				r.resolve( response );
			},
			error: function( response ) {
				r.reject( response );
			}
		});

		return r.promise;
	}

	function getFrontPageVideos() {
		var r = Q.defer();

		$.ajax({
			url: serverAddress,
			dataType: "jsonp",
			crossDomain: true,
			data: {
				t: "getFrontPage"
			},
			success: function( response ) {
				r.resolve( response );
			},
			error: function( response ) {
				r.reject( response );
			}
		});
		return r.promise;
	}

	app.promisedQuery = promisedQuery;

	function promisedQuery( data ) {
		return function( args ) {
			var r = Q.defer();
			$.ajax({
				url: serverAddress,
				dataType: "jsonp",
				crossDomain: true,
				data: data,
				success: function( response ) {
					r.resolve( response );
				},
				error: function( response ) {
					r.reject( response );
				}
			});
			return r.promise;
		}
	}

	app.toggleFrontPage = function ( speed ) {
		$('#row_0').toggle( num, function( speed ) { $('#row_1').toggle( speed ) } );
	}

	return app;
}());
