package client;

import java.util.List;

public class YoutubeVideo {
	
	private List<String> thumbnails;
	private List<YoutubeMedia> medias;
	private String webPlayerUrl;
	private String embeddedWebPlayerUrl;
	//UUTTA JUTTUA, JOKA ROMUTTAA KAIKEN
	private String title;
	private String duration;
	private String description;
	private String videoId;
	
	public String getVideoId() {
		return videoId;
	}
	
	public void setVideoId(String id) {
		this.videoId = id;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String desc) {
		this.description = desc;
	}
	
	public String getDuration() {
		return duration;
	}
	
	public void setDuration(String dur) {
		this.duration = dur;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String t) {
		this.title = t;
	}
	
	//UUDET JUTUT LOPPUU
	
	public List<String> getThumbnails() {
		return thumbnails;
	}
	
	public void setThumbnails(List<String> thumbnails) {
		this.thumbnails = thumbnails;
	}

	public List<YoutubeMedia> getMedias() {
		return medias;
	}
	public void setMedias(List<YoutubeMedia> medias) {
		this.medias = medias;
	}
	
	public String getWebPlayerUrl() {
		return webPlayerUrl;
	}
	
	public void setWebPlayerUrl(String webPlayerUrl) {
		this.webPlayerUrl = webPlayerUrl;
	}

	public String getEmbeddedWebPlayerUrl() {
		return embeddedWebPlayerUrl;
	}
	
	public void setEmbeddedWebPlayerUrl(String embeddedWebPlayerUrl) {
		this.embeddedWebPlayerUrl = embeddedWebPlayerUrl;
	}
	
	public String retrieveHttpLocation() {
		if (medias==null || medias.isEmpty()) {
			return null;
		}
		for (YoutubeMedia media : medias) {
			String location = media.getLocation();
			if (location.startsWith("http")) {
				return location;
			}
		}
		return null;
	}

}
