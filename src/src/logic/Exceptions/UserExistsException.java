package logic.Exceptions;

@SuppressWarnings("serial")
public class UserExistsException extends Exception {
	
	public UserExistsException() {
		super();
	}
	
	public UserExistsException(String message) {
		super(message);
	}
	
	public UserExistsException(String message, Throwable cause) {
		super(message, cause);
	}

}
