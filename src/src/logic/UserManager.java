package logic;

import database.*;
import database.Query.AddUserQuery;
import database.Query.DeleteUserQuery;
import database.Query.GetClientSaltQuery;
import database.Query.GetServerHashQuery;
import database.Query.GetServerSaltQuery;
import database.Query.UserExistsQuery;

import java.util.ArrayList;
import java.io.IOException;
import java.sql.*;

import logic.Exceptions.InvalidPasswordException;
import logic.Exceptions.NoSuchUserException;
import logic.Exceptions.UserExistsException;
import sun.misc.BASE64Decoder;

public class UserManager {
	
	private DBConnection db;
	
	public UserManager(DBConnection db) {
		this.db = db;
	}

	public void addUser(String userEmail, String clientPasswordHash, String clientPasswordSalt) throws UserExistsException {
		/*
		 * client pw and salt are stored as plain varchars
		 * server pw and salt are stored as base64 encoded varchars
		 */
		
		if (userExists(userEmail)) {
			throw new UserExistsException(userEmail);
		} 
		
		// generate a new password here based on the hash provided by the 
		PasswordHash pw = new PasswordHash();
		pw.Hash(clientPasswordHash);
		final String serverPasswordHash = pw.getDigest();
		final String serverPasswordSalt = pw.getSalt();
		
		AddUserQuery query = new AddUserQuery(db.getDbConnection(),
											  userEmail,
											  serverPasswordHash, 
											  serverPasswordSalt, 
											  clientPasswordHash, 
											  clientPasswordSalt);
		query.execute();
		query.close();

	}
	
	public void deleteUser(String userEmail) {
		if (userExists(userEmail)) {
			DeleteUserQuery query = new DeleteUserQuery(db.getDbConnection(), userEmail);
			query.execute();
			query.close();
		}
	}
	
	public boolean userExists(String userName) {
		try {
			UserExistsQuery query = new UserExistsQuery(db.getDbConnection(), userName);
			query.execute();
			query.getResults().next();
			boolean b = query.getResults().getBoolean(1);
			query.close();
			return b;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

	}
	
	public boolean authenticateUser(String userName, String clientPasswordHash) throws InvalidPasswordException, NoSuchUserException {
		if (!userExists(userName)) {
			throw new NoSuchUserException();
		}
		
		try {
			GetServerSaltQuery serverSaltQuery = new GetServerSaltQuery(db.getDbConnection(), userName);
			serverSaltQuery.execute();
			serverSaltQuery.getResults().next();
			final String sSalt = serverSaltQuery.getResults().getString(1);
			serverSaltQuery.close();
			
			PasswordHash pw = new PasswordHash();
			try {
				pw.Hash(clientPasswordHash, PasswordHash.base64ToByte(sSalt));
			} catch (IOException e) {
				e.printStackTrace();
			}
			String serverPasswordHash = pw.getDigest();
			
			GetServerHashQuery serverHashQuery = new GetServerHashQuery(db.getDbConnection(), userName);
			serverHashQuery.execute();
			serverHashQuery.getResults().next();
			final String serverHash = serverHashQuery.getResults().getString(1);
			serverHashQuery.close();
			
			if (!serverHash.equals(serverPasswordHash)) {
				throw new InvalidPasswordException();
			} else {
				return true;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
	}
	
	/*
	 * Get user's salt value from DB
	 */
	public String getUserClientSalt(String userName) throws NoSuchUserException {
		String clientSalt = null;
		
		try {
		GetClientSaltQuery clientSaltQuery = new GetClientSaltQuery(db.getDbConnection(), userName);
		clientSaltQuery.execute();
		clientSaltQuery.getResults().next();
		clientSalt = clientSaltQuery.getResults().getString(1);
		} catch (SQLException e) {
			throw new NoSuchUserException( userName );
		}
		
		return clientSalt;
	}
}
