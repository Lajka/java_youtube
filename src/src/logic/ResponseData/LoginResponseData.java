package logic.ResponseData;


public class LoginResponseData implements ResponseData {

	private boolean ok;
	private boolean invalidUser;
	private boolean invalidPassword;
	
	public LoginResponseData(boolean ok, boolean invalidUser, boolean invalidPassword) {
		this.ok = ok;
		this.invalidUser = invalidUser;
		this.invalidPassword = invalidPassword;
	}

	@Override
	public String getResponseData() {
		return new String("ok: " + ok + "; invalidUser: " + invalidUser + "; invalidPassword: " + invalidPassword + ";");
	}

	public boolean isInvalidUser() {
		return invalidUser;
	}

	public void setInvalidUser(boolean invalidUser) {
		this.invalidUser = invalidUser;
	}

	public boolean isInvalidPassword() {
		return invalidPassword;
	}

	public void setInvalidPassword(boolean invalidPassword) {
		this.invalidPassword = invalidPassword;
	}

	public boolean isOk() {
		return ok;
	}

	public void setOk(boolean ok) {
		this.ok = ok;
	}

}
