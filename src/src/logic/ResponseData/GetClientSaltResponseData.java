package logic.ResponseData;


public class GetClientSaltResponseData implements ResponseData {
	
	private String clientSalt;

	public GetClientSaltResponseData(String clientSalt) {
		this.clientSalt = clientSalt;
	}
	
	@Override
	public String getResponseData() {
		return clientSalt;
	}
	
}
