package logic.ResponseData;


public class UserExistsResponseData implements ResponseData {

	private String responseData;
	
	public UserExistsResponseData(String userName) {
		responseData = userName;
	}

	@Override
	public String getResponseData() {
		return responseData;
	}

}
