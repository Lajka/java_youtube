package logic;

import com.google.gson.Gson;

public class JsonResponse {
	
	private String responseData;
	private String response;
	private String callback;
	
	public JsonResponse (String callback, Object object) {
		Gson gson = new Gson();
		this.callback = callback;
		this.responseData = gson.toJson(object);	
		response = callback + "(" + responseData + ")";
	}
	
	public String getResponse() {
		return response;
	}

}