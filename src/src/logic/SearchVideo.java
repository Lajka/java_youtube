package logic;


import client.Youtube;
import client.YoutubeVideo;

import java.util.ArrayList;
import java.util.List;

/*
 * Hakee youtube -videoita annetulla hakusanalla.
 * Listaa haetut videot videos -listaan, jota voidaan k�ytt�� muualla.
 * Esim. AjaxSearchVideo.java k�ytt�� videos -listaa ja vie sen sis�lt�m�t video tietokantaan.
 * 
 * Tuottaa haetuista videoista my�s videData -lista, jota k�ytt�� AjaxGetFrontPage.java
 * AIKAMOINEN KOMMENTTI, EN TIED� MIT� TEEN
 */
public class SearchVideo {
	
	private String searchTerm;
	private int maxResults;
	private boolean filter;
	private int timeout;
	private Youtube youtube = new Youtube("aggregator");
	private List<YoutubeVideo> videos = null;
	private ArrayList<VideoData> videoData = null;
	
	// Konstruktori pelk�ll� hakutermill�, palautettujen tulosten maksimim��r� vakiona 6.
	public SearchVideo(String st) {
		this.searchTerm = st;
		this.maxResults = 6;
		this.filter = true;
		this.timeout = 2000;
		this.videoData = new ArrayList<VideoData>();

		listVideos();
		
	}
	
	// Konstruktori hakutermill� ja tulosten maksimim��r�ll�
	public SearchVideo(String st, int max) {
		this.searchTerm = st;
		this.maxResults = max;
		this.filter = true;
		this.timeout = 2000;
		this.videoData = new ArrayList<VideoData>();
		
		listVideos();
	}
	
	private void listVideos() {
		try {
			videos = youtube.retrieveVideos(searchTerm, maxResults, filter, timeout);
			for (YoutubeVideo y : videos) {
				videoData.add(new VideoData(y.getThumbnails().get(1), y.getEmbeddedWebPlayerUrl(), y.getDescription(), y.getTitle(), y.getDuration(), searchTerm, y.getVideoId()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<YoutubeVideo> getVideos() {
		return videos;
	}
	
	public ArrayList<VideoData> getVideoData() {
		return videoData;
	}
	
	public String getSearchTerm() {
		return searchTerm;
	}

}
