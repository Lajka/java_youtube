package database.Query;

import java.sql.*;

public class GetClientHashQuery implements Query {

	private PreparedStatement stmt = null;
	private ResultSet rs = null;
	
	public GetClientHashQuery(Connection conn, String userEmail) {
		try {
			stmt = conn.prepareStatement("SELECT pw_client_hash FROM users WHERE user_name = ? LIMIT 1");
			stmt.setString(1, userEmail);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void execute() {
		try {
			rs = stmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public ResultSet getResults() {
		return rs;
	}
	
	public void close() {
		try {
			stmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
