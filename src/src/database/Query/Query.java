package database.Query;

public interface Query {
	void execute();
	void close();
}
