package httpserver;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.util.Map;

import logic.Logic;
import logic.Exceptions.NoSuchUserException;
import logic.Exceptions.UserExistsException;
import logic.ResponseData.NoSuchUserResponseData;
import logic.ResponseData.UserExistsResponseData;


public class Handler implements HttpHandler {
	private Logic logic;
	private Server server;
	
	public Handler(Server server, Logic logic) {
		this.logic = logic;
		this.server = server;
	}
	
	public void handle(HttpExchange t) throws IOException {
		Map<String, String> params = server.queryToMap(t.getRequestURI().getQuery());
		if (params.containsKey("callback") && params.containsKey("t")) {
			
			final String type = params.get("t");
			
			try {
				if (type.equals("login")) {
					AjaxLogin a = new AjaxLogin(server, logic, params);
					System.out.println(a.getResponse());
					Server.writeResponse(t, a.getResponse());
				} else if (type.equals("registerUser")) {
					AjaxRegisterUser a = new AjaxRegisterUser(server, logic, params);
					System.out.println(a.getResponse());
				} else if (type.equals("getClientSalt")) {
					AjaxGetClientSalt a = new AjaxGetClientSalt(server, logic, params);
					System.out.println(a.getResponse());
					Server.writeResponse(t, a.getResponse());
				} else if (type.equals("searchVideo")) {
					AjaxSearchVideo a = new AjaxSearchVideo(server, logic, params);
					Server.writeResponse(t, a.getResponse());
				} else if (type.equals("getVideo")) {
					AjaxGetVideo a = new AjaxGetVideo(server, logic, params);
					Server.writeResponse(t, a.getResponse());
				} else if (type.equals("getFrontPage")) {
					AjaxGetFrontPageVideos a = new AjaxGetFrontPageVideos(server, logic, params);
					Server.writeResponse(t, a.getResponse());
				} else if (type.equals("showMeEverything")) {
					AjaxGetAllVideos a = new AjaxGetAllVideos(server, logic, params);
					Server.writeResponse(t, a.getResponse());
				} else {
					System.out.println("You dun goofed");
				}
			} catch (UserExistsException uee) {
				final String msg = uee.getMessage();
				UserExistsResponseData rsp = null;
				
				if ( msg != null ) {
					rsp = new UserExistsResponseData(msg);
				} else {
					rsp = new UserExistsResponseData("Unknown user");
				}
				
				Server.writeResponse(t, rsp.getResponseData());
				
			} catch (NoSuchUserException e) {
				final String msg = e.getMessage();
				NoSuchUserResponseData rsp = null;
				
				if ( msg != null ) {
					rsp = new NoSuchUserResponseData( msg );
				} else { 	
					rsp = new NoSuchUserResponseData( "Unknown user" );
				}
								
				Server.writeResponse(t, rsp.getResponseData());	
			}
						
		} else {
			// invalid query	
			// package into json and send response throught server
			// 
			System.out.println("You don goofed");
		}
	}
}

