package httpserver;

import java.util.Map;

import logic.JsonResponse;
import logic.Logic;
import logic.UserManager;
import logic.Exceptions.NoSuchUserException;
import logic.ResponseData.GetClientSaltResponseData;

public class AjaxGetClientSalt extends AjaxParser {
	
	private String salt;
	private String response;

	public AjaxGetClientSalt(Server server, Logic logic, Map<String, String> map) throws NoSuchUserException {
		super(server, logic, map);
		final String userName = map.get("u");
		this.salt = null; 
				
		UserManager manager = logic.getUserManager();

		this.salt = manager.getUserClientSalt(userName);
		
		GetClientSaltResponseData responseData = new GetClientSaltResponseData(getSalt());
		JsonResponse resp = new JsonResponse(getCallback(), responseData);
		
		this.response = resp.getResponse();
	}
	
	public String getSalt() {
		return this.salt;
	}
	
	public String getResponse() {
		return this.response;
	}

}
