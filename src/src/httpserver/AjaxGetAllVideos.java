package httpserver;

import java.sql.SQLException;
import java.util.Map;
import java.util.ArrayList;

import database.DBConnection;
import database.Query.GetAllVideosQuery;

import logic.Logic;
import logic.DisplayVideos;

public class AjaxGetAllVideos extends AjaxParser {
	
	private String response;
	private ArrayList<DisplayVideos> videos;
	
	public AjaxGetAllVideos(Server server, Logic logic, Map<String, String> map) {
		super(server, logic, map);
		DBConnection db = logic.getDb();
		this.videos = new ArrayList<DisplayVideos>();
		this.response = "";
		
		try {
			GetAllVideosQuery allVideosQuery = new GetAllVideosQuery(db.getDbConnection());
			allVideosQuery.execute();
			while (allVideosQuery.getResults().next()) {
				int id = allVideosQuery.getResults().getInt(1);
				String turl = allVideosQuery.getResults().getString(2);
				String vurl = allVideosQuery.getResults().getString(3);
				String des = allVideosQuery.getResults().getString(4);
				String tit = allVideosQuery.getResults().getString(5);
				String dur = allVideosQuery.getResults().getString(6);
				String sea = allVideosQuery.getResults().getString(7);
				String vid = allVideosQuery.getResults().getString(8);
				videos.add(new DisplayVideos(id, turl, vurl, des, tit, dur, sea, vid));
			}
			
			allVideosQuery.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		this.response += videos;
	}
	
	public String getResponse() {
		return response;
	}
	
	public ArrayList<DisplayVideos> getVideos() {
		return videos;
	}

}
