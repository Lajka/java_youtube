package httpserver;

import java.util.Map;

import logic.*;
import logic.Exceptions.InvalidPasswordException;
import logic.Exceptions.NoSuchUserException;
import logic.ResponseData.LoginResponseData;

public class AjaxLogin extends AjaxParser {
	
	private String user;
	private String password;
	private String response;

	public AjaxLogin(Server server, Logic logic, Map<String, String> map) {
		super(server, logic, map);
		this.user = map.get("u");
		this.password = map.get("p");
		UserManager manager = logic.getUserManager();
		
		logic.ResponseData.LoginResponseData responseData = new logic.ResponseData.LoginResponseData (true, false, false);
		try {
			manager.authenticateUser(getUser(), getPassword());
		} catch (NoSuchUserException nsee) {
			responseData.setInvalidUser(true);
			responseData.setInvalidPassword(false);
			responseData.setOk(false);
		} catch (InvalidPasswordException ipe) {
			responseData.setInvalidUser(false);
			responseData.setInvalidPassword(true);
			responseData.setOk(false);
		} finally {
			JsonResponse resp = new JsonResponse(getCallback(), responseData);
			this.response = resp.getResponse();
		}
	}
	
	public String getUser() {
		return this.user;
	}
	
	public String getPassword() {
		return this.password;
	}
	
	public String getResponse() {
		return this.response;
	}

}
