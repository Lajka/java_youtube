package app;

import logic.*;
import database.*;
import httpserver.*;
import client.*;

public class Main {
	static DBConnection db;
	
	public static void main(String args[]) {
		
		db = new DBConnection();
		
		db.connect();
		
		Logic logic = new Logic(db);
		Server server = new Server();
		
		try {
			server.startServer(logic);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	@Override
	protected void finalize() throws Throwable {
		db.disconnect();
	}

}
